package user

import (
	"github.com/bwmarrin/discordgo"
)

// AddRole to add roles to a user in relation to the reactions of a message
func AddRole(s *discordgo.Session, m *discordgo.MessageReactionAdd) {
	if m.MessageID != "563634765098647553" {
		return
	}
	if m.Emoji.Name == "games" {
		s.GuildMemberRoleAdd(m.GuildID, m.UserID, "563636156210741248")
	} else if m.Emoji.Name == "system" {
		s.GuildMemberRoleAdd(m.GuildID, m.UserID, "563636125403447296")
	} else if m.Emoji.Name == "web" {
		s.GuildMemberRoleAdd(m.GuildID, m.UserID, "563636099184853002")
	}
}

// RemoveRole to remove roles to a user in relation to the reactions of a message
func RemoveRole(s *discordgo.Session, m *discordgo.MessageReactionRemove) {
	if m.MessageID != "563634765098647553" {
		return
	}
	if m.Emoji.Name == "games" {
		s.GuildMemberRoleRemove(m.GuildID, m.UserID, "563636156210741248")
	} else if m.Emoji.Name == "system" {
		s.GuildMemberRoleRemove(m.GuildID, m.UserID, "563636125403447296")
	} else if m.Emoji.Name == "web" {
		s.GuildMemberRoleRemove(m.GuildID, m.UserID, "563636099184853002")
	}
}
