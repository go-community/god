package user

import "github.com/bwmarrin/discordgo"

// HelpCommand for see all commands
func HelpCommand(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Content == "&help" {
		helpCommand := discordgo.MessageEmbedField{Name: "Commande d'aide", Value: "Affiche ce message d'aide", Inline: true}

		helpMessage := discordgo.MessageEmbed{
			Title:  "Message d'aide",
			Fields: []*discordgo.MessageEmbedField{&helpCommand},
		}
		s.ChannelMessageSendEmbed(m.ChannelID, &helpMessage)
	}
}
