package user

import (
	"log"

	"github.com/bwmarrin/discordgo"
)

// WelcomeMessage to welcome a member
func WelcomeMessage(s *discordgo.Session, g *discordgo.GuildMemberAdd) {
	mpChannel, err := s.UserChannelCreate(g.User.ID)

	if err != nil {
		log.Println("cannot open private channel ", err)
		return
	}

	defer s.ChannelMessageSend("562993808522870786", "Bienvenue à <@"+g.Member.User.ID+">"+" fait ta présentation :)")

	s.ChannelMessageSend(mpChannel.ID, "Bienvenue à toi dans la communauté ! Aujourd'hui nous t'offrons l'accès à une communauté de personnes passionnée du langage Go et voulant aider et participer à des discussions autour de celui-ci ! Nous te demandons simplement de respecter les principales règles de base du savoir-être ! Pour découvrir mes commandes, dit moi &help ici ou dans le channel bots du discord")
	s.GuildMemberRoleAdd(g.GuildID, g.User.ID, "563084539740684316")
}
