package main

import (
	"math/rand"
	"time"

	staff "./staff"
	user "./user"
	"github.com/bwmarrin/discordgo"
)

func ready(s *discordgo.Session, event *discordgo.Ready) {
	answers := []string{
		"Surveiller le monde",
		"Diriger le monde",
		"Développer en PHP",
		"Apprendre Javascript",
		"Ban des membres",
		"Regarder GravenTV",
		"Joue à Minecraft",
		"Participer à la conversion",
		"Diriger le monde",
		"Chercher un lapin",
		"Manger des bonbons",
		"Voir AkameGaKille",
		"Suicide ...",
		"Dépression ...",
		"Faire un réacteur",
		"Devenir président",
		"Manger du fromage",
	}

	ticker := time.NewTicker(60 * time.Second)
	quit := make(chan struct{})
	go func() {
		for {
			select {
			case <-ticker.C:
				s.UpdateStatus(0, answers[rand.Intn(len(answers))])
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()
}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {

	if m.Author.ID == s.State.User.ID {
		return
	}

	user.HelpCommand(s, m)
	staff.ClearCommand(s, m)
	staff.MuteCommand(s, m)

}

func guildMemberAdd(s *discordgo.Session, g *discordgo.GuildMemberAdd) {
	user.WelcomeMessage(s, g)
}

func messageReactionAdd(s *discordgo.Session, m *discordgo.MessageReactionAdd) {
	user.AddRole(s, m)
}

func messageReactionRemove(s *discordgo.Session, m *discordgo.MessageReactionRemove) {
	user.RemoveRole(s, m)
}
