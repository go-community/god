package staff

import (
	"log"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

// MuteCommand to silence the bad limbs
func MuteCommand(s *discordgo.Session, m *discordgo.MessageCreate) {
	if strings.HasPrefix(m.Content, "&mute") {
		for _, staffUsername := range staffMembers {
			if staffUsername != m.Author.Username {
				s.ChannelMessageSend(m.ChannelID, "Tu n'as pas la permission")
				return
			}
		}

		userID := strings.Split(m.Content, " ")[1]
		userID = strings.Split(userID, "<@")[1]
		userID = strings.Split(userID, ">")[0]

		s.GuildMemberRoleAdd(m.GuildID, userID, "563644193029750787")

		number := strings.Split(m.Content, " ")[2]
		delay, err := time.ParseDuration(number + "m")

		if err != nil {
			log.Println("Error on time parse duration ", err)
			return
		}

		s.ChannelMessageSend(m.ChannelID, "<@"+userID+">"+" à était mute pendant "+number+" minutes")

		ticker := time.NewTicker(delay)
		quit := make(chan struct{})
		go func() {
			for {
				select {
				case <-ticker.C:
					s.GuildMemberRoleRemove(m.GuildID, userID, "563644193029750787")
					s.ChannelMessageSend(m.ChannelID, "<@"+userID+">"+" à était unmute")
					ticker.Stop()
					return
				case <-quit:
					ticker.Stop()
					return
				}
			}
		}()
	}
}
