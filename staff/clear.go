package staff

import (
	"log"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
)

// ClearCommand to clean up a clear quantity of messages
func ClearCommand(s *discordgo.Session, m *discordgo.MessageCreate) {
	if strings.HasPrefix(m.Content, "&clear") {

		for _, staffUsername := range staffMembers {
			if staffUsername != m.Author.Username {
				s.ChannelMessageSend(m.ChannelID, "Tu n'as pas la permission")
				return
			}
		}

		numberClear, err := strconv.Atoi(strings.Split(m.Content, " ")[1])
		if err != nil {
			log.Println("Error on conversion string to int for numberClear ", err)
			return
		}

		numberClear++

		messagesDelete, err := s.ChannelMessages(m.ChannelID, numberClear, "", "", "")
		if err != nil {
			log.Println("Error on messagesDelete ", err)
			return
		}

		for _, e := range messagesDelete {
			s.ChannelMessageDelete(m.ChannelID, e.ID)
		}

	}

}
