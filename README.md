## God

Gond est un bot discord pour la communauté permettant la gestion rapide du discord autant par les membres que pour les administrateurs en répondant à diverses commandes :

- Message pour les nouveaux arrivants
- Message en cas de départ
- Ajout de rôle automatique
- Musique via Youtube
- Dernières nouveautés du langage go
- Sondages
- Commande d'administration (ban, mute, kick, clear)